package org.pneditor.petrinet.adapters.imta;

import org.pneditor.petrinet.AbstractTransition;

import Classes.Transition;

public class TransitionAdapter extends AbstractTransition {

	private Transition t;
	
	public TransitionAdapter(String label) {
		super(label);
		this.t = new Transition(Integer.parseInt(label));
	}
	
	public Transition getTransition() {
		return this.t;
	}
}
