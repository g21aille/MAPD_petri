package org.pneditor.petrinet.adapters.imta;

import org.pneditor.petrinet.AbstractArc;
import org.pneditor.petrinet.AbstractNode;
import org.pneditor.petrinet.ResetArcMultiplicityException;

import Classes.Arc;
import Classes.ArcReset;
import Classes.ArcZero;

public class ArcVideurAdapter extends AbstractArc {
	private Arc a;
	private AbstractNode source;
	private AbstractNode destination;
	
	public Arc getArc() {
		return this.a;
	}
	
	public ArcVideurAdapter(AbstractNode source,AbstractNode destination) {
		this.source = source;
		this.destination = destination;
		this.a = new ArcReset(((PlaceAdapter) source).getPlace(),((TransitionAdapter) destination).getTransition());
	
	}
	
	@Override
	public AbstractNode getSource() {
		return this.source;
	}

	@Override
	public AbstractNode getDestination() {
		// TODO Auto-generated method stub
		return this.destination;
	}

	@Override
	public boolean isReset() {
		return true;
	}

	@Override
	public boolean isRegular() {
		return false;
	}

	@Override
	public boolean isInhibitory() {
		return false;
	}

	@Override
	public int getMultiplicity() throws ResetArcMultiplicityException {
		return (int) this.a.getWeigth();
	}

	@Override
	public void setMultiplicity(int multiplicity) throws ResetArcMultiplicityException {
		this.a.setWeigth(multiplicity);
		
	}

}
