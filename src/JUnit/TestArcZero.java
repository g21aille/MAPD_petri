package JUnit;

import Classes.*;
import org.junit.Assert.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Order;

public class TestArcZero {
	@Test
	public void FireArcZero() {
		ReseauPetri rp = new ReseauPetri();
		Place p1 = new Place(3,1);
		Place p2 = new Place(0,2);
		Place p3 = new Place(0,3);
		Place p4 = new Place (0,4);
		Transition t1 = new Transition(1);
		Transition t2 = new Transition(2);
		ArcZero az1 = new ArcZero(1,p1,t1);
		ArcZero az2 = new ArcZero(1,p3,t2);
		Arc a1 = new Arc (1,p2,1,t1);
		Arc a2 = new Arc (1,p4,1,t2);
		rp.Add(p1);
		rp.Add(p2);
		rp.Add(az1);
		rp.Add(a1);
		rp.Fire(t1);
		assertEquals((int) p1.getTokens(),3);
		rp.Add(p3);
		rp.Add(p4);
		rp.Add(az2);
		rp.Add(a2);
		rp.Fire(t2);
		assertEquals((int) p3.getTokens(),0);
		assertEquals((int) p4.getTokens(),1);
	}
	@Test
	public void isActivable() {
		Place p1 = new Place(0,1);
		Place p2 = new Place(3,2);
		Transition t = new Transition(1);
		ArcZero az1 = new ArcZero(1,p1,t);
		ArcZero az2 = new ArcZero(1,p2,t);
		assertTrue(az1.isActivable());
		assertFalse(az2.isActivable());
	}

}
