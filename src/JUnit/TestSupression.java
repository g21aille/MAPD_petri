package JUnit;
import Classes.*;
import org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Order;


public class TestSupression {
	
	@Test
	public void ST2() throws Exception{
		ReseauPetri r = new ReseauPetri();
		Place p1 = new Place(2,0);
		Place p2 = new Place(2,0);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(1,p2,0,t1);
		Arc a2 = new Arc(1,p1,1,t1);
		t1.getArcList().add(a1);
		t1.getArcList().add(a2);
		r.Add(p2);
		r.Add(a2);
		r.Add(a1);
		r.Add(p1);
		r.Add(t1);
		r.Remove(t1);
		assertEquals(r.getArcList().size(),0);
		assertEquals(r.getTransitionList().size(),0);
	}
	
	@Test
	public void SA1() throws Exception{
		ReseauPetri r = new ReseauPetri();
		Place p1 = new Place(2,0);
		Place p2 = new Place(2,0);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(1,p2,0,t1);
		Arc a2 = new Arc(1,p1,1,t1);
		t1.getArcList().add(a1);
		t1.getArcList().add(a2);
		r.Add(p2);
		r.Add(a2);
		r.Add(a1);
		r.Add(p1);
		r.Add(t1);
		r.Remove(a1);
		assertEquals(r.getArcList().size(),1);
		assertEquals(r.getTransitionList().size(),1);
		assertEquals(r.getPlaceList().size(),2);
	}
	
	@Test
	public void SP1() throws Exception{
		ReseauPetri r = new ReseauPetri();
		Place p1 = new Place(2,0);
		Place p2 = new Place(2,0);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(1,p2,0,t1);
		Arc a2 = new Arc(1,p1,1,t1);
		t1.getArcList().add(a1);
		t1.getArcList().add(a2);
		r.Add(p2);
		r.Add(a2);
		r.Add(a1);
		r.Add(p1);
		r.Add(t1);
		r.Remove(p1);
		assertEquals(r.getArcList().size(),1);
		assertEquals(r.getTransitionList().size(),1);
		assertEquals(r.getPlaceList().size(),1);
	}
	
	@Test
	public void SMA0() throws Exception{
		ReseauPetri r = new ReseauPetri();
		Place p1 = new Place(2,0);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(1,p1,0,t1);
		Arc a2 = new Arc(1,p1,0,t1);
		t1.getArcList().add(a1);
		t1.getArcList().add(a2);
		r.Add(a2);
		r.Add(a1);
		r.Add(p1);
		r.Add(t1);
		r.Remove(p1);
		assertEquals(r.getArcList().size(),0);
		assertEquals(r.getTransitionList().size(),1);
		assertEquals(r.getPlaceList().size(),0);
	}
}
