package JUnit;

import Classes.*;
import org.junit.Assert.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Order;

public class TestArcVideur {
	@Test
	public void FireArcVideur() {
		ReseauPetri rp = new ReseauPetri();
		Place p1 = new Place(3,1);
		Place p2 = new Place(0,2);
		Transition t = new Transition(1);
		ArcReset az = new ArcReset(p1,t);
		Arc a = new Arc (1,p2,1,t);
		rp.Add(p1);
		rp.Add(p2);
		rp.Add(az);
		rp.Add(a);
		rp.Fire(t);
		assertEquals((int) p1.getTokens(),0);
		assertEquals((int) p2.getTokens(),1);
	}
	@Test
	public void isActivable() {
		Place p1 = new Place(0,1);
		Place p2 = new Place(3,2);
		Transition t = new Transition(1);
		ArcReset av1 = new ArcReset(p1,t);
		ArcReset av2 = new ArcReset(p2,t);
		assertTrue(av2.isActivable());
		assertFalse(av1.isActivable());
	}
	@Test
	public void removeToken() {
		Place p = new Place(0,1);
		Transition t = new Transition(1);
		ArcReset av = new ArcReset(p,t);
		av.removeTokens(p);
		assertEquals((int) p.getTokens(),0);
	}
}
