package JUnit;
import Classes.*;


import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.jupiter.api.Order;

public class TestAffichage {
	@Test
	@Order(1)
	public void AT1() throws Exception{
		ReseauPetri t = new ReseauPetri();
		Transition t1 = new Transition(0);
		t.Add(t1);
		String res = t.printRDP();
		String resAttendus= "-------------------------------------------------------\n"
				+ "Réseau de Petri\n"
				+ "  0 places\n"
				+ "  1 transitions\n"
				+ "  0 arcs\n"
				+ "\n"
				+ "Liste de places :\n"
				+ "\n"
				+ "Liste des Transitions : \n"
				+ "  transition T0\n"
				+ "\n"
				+ "Liste des Arcs :\n"
				+ "-------------------------------------------------------";
		assertEquals(res,resAttendus);
		
	}
	@Test
	@Order(3)
	public void AT3() throws Exception{
		ReseauPetri t = new ReseauPetri();
		Transition t1 = new Transition(0);
		Place p2 = new Place(1,1);
		Arc a2 = new Arc(1,p2,1,t1);
		t.Add(t1);
		t.Add(a2);
		t.Add(p2);
		String res = t.printRDP();
		String resAttendus = "-------------------------------------------------------\n"
				+ "Réseau de Petri\n"
				+ "  1 places\n"
				+ "  1 transitions\n"
				+ "  1 arcs\n"
				+ "\n"
				+ "Liste de places :\n"
				+ "  place P0 avec 1.0 jetons\n"
				+ "\n"
				+ "Liste des Transitions : \n"
				+ "  transition T0\n"
				+ "\n"
				+ "Liste des Arcs :\n"
				+ "  Arc       n·0 T0 - 1.0 -> P1\n"
				+ "-------------------------------------------------------";
		assertEquals(res,resAttendus);
	}
	@Test
	@Order(2)
	public void AT2() throws Exception{
		ReseauPetri t = new ReseauPetri();
		Transition t1 = new Transition(0);
		Place p2 = new Place(1,1);
		Arc a2 = new Arc(1,p2,0,t1);
		t.Add(t1);
		t.Add(a2);
		t.Add(p2);
		String res = t.printRDP();
		String resAttendus = "-------------------------------------------------------\n"
				+ "Réseau de Petri\n"
				+ "  1 places\n"
				+ "  1 transitions\n"
				+ "  1 arcs\n"
				+ "\n"
				+ "Liste de places :\n"
				+ "  place P0 avec 1.0 jetons\n"
				+ "\n"
				+ "Liste des Transitions : \n"
				+ "  transition T0\n"
				+ "\n"
				+ "Liste des Arcs :\n"
				+ "  Arc       n·0 P1 - 1.0 -> T0\n"
				+ "-------------------------------------------------------";
		assertEquals(res,resAttendus);
	}
	
	@Test
	@Order(4)
	public void AT4() throws Exception{
		ReseauPetri t = new ReseauPetri();
		Place p1 = new Place(1,0);
		Place p2 = new Place(1,1);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(1,p1,1,t1);
		Arc a2 = new Arc(1,p2,0,t1);
		t1.getArcList().add(a1);
		t1.getArcList().add(a2);
		t.Add(a1);
		t.Add(a2);
		t.Add(p1);
		t.Add(p2);
		t.Add(t1);
		String res = t.printRDP();
		String resAttendus = "-------------------------------------------------------\n"
				+ "Réseau de Petri\n"
				+ "  2 places\n"
				+ "  1 transitions\n"
				+ "  2 arcs\n"
				+ "\n"
				+ "Liste de places :\n"
				+ "  place P0 avec 1.0 jetons\n"
				+ "  place P1 avec 1.0 jetons\n"
				+ "\n"
				+ "Liste des Transitions : \n"
				+ "  transition T0\n"
				+ "\n"
				+ "Liste des Arcs :\n"
				+ "  Arc       n·0 T0 - 1.0 -> P0\n"
				+ "  Arc       n·1 P1 - 1.0 -> T0\n"
				+ "-------------------------------------------------------";
		assertEquals(res,resAttendus);
	}
	
	@Test
	@Order(5)
	public void AP1() throws Exception{
		ReseauPetri t = new ReseauPetri();
		Place p1 = new Place(0,0);
		t.Add(p1);
		String res = t.printRDP();
		String resAttendus = "-------------------------------------------------------\n"
				+ "Réseau de Petri\n"
				+ "  1 places\n"
				+ "  0 transitions\n"
				+ "  0 arcs\n"
				+ "\n"
				+ "Liste de places :\n"
				+ "  place P0 avec 0.0 jetons\n"
				+ "\n"
				+ "Liste des Transitions : \n"
				+ "\n"
				+ "Liste des Arcs :\n"
				+ "-------------------------------------------------------";
		assertEquals(res,resAttendus);
	}
	@Test
	@Order(6)
	public void AP2() throws Exception{
		ReseauPetri t = new ReseauPetri();
		Place p1 = new Place(2,0);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(1,p1,1,t1);
		t1.getArcList().add(a1);
		t.Add(p1);
		t.Add(a1);
		t.Add(t1);
		String resAttendus = "-------------------------------------------------------\n"
				+ "Réseau de Petri\n"
				+ "  1 places\n"
				+ "  1 transitions\n"
				+ "  1 arcs\n"
				+ "\n"
				+ "Liste de places :\n"
				+ "  place P0 avec 2.0 jetons\n"
				+ "\n"
				+ "Liste des Transitions : \n"
				+ "  transition T0\n"
				+ "\n"
				+ "Liste des Arcs :\n"
				+ "  Arc       n·0 T0 - 1.0 -> P0\n"
				+ "-------------------------------------------------------";
		String res = t.printRDP();
		assertEquals(res,resAttendus);
		
	}
	@Test
	@Order(6)
	public void AP3() throws Exception{
		ReseauPetri t = new ReseauPetri();
		Place p1 = new Place(4,0);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(1,p1,0,t1);
		t1.getArcList().add(a1);
		t.Add(p1);
		t.Add(a1);
		t.Add(t1);
		String resAttendus = "-------------------------------------------------------\n"
				+ "Réseau de Petri\n"
				+ "  1 places\n"
				+ "  1 transitions\n"
				+ "  1 arcs\n"
				+ "\n"
				+ "Liste de places :\n"
				+ "  place P0 avec 4.0 jetons\n"
				+ "\n"
				+ "Liste des Transitions : \n"
				+ "  transition T0\n"
				+ "\n"
				+ "Liste des Arcs :\n"
				+ "  Arc       n·0 P0 - 1.0 -> T0\n"
				+ "-------------------------------------------------------";
		String res = t.printRDP();
		assertEquals(res,resAttendus);
		
	}
	@Test
	@Order(7)
	public void AP4() throws Exception{
		ReseauPetri t = new ReseauPetri();
		Place p1 = new Place(1,0);
		Transition t1 = new Transition(0);
		Transition t2 = new Transition(1);
		Arc a1 = new Arc(1,p1,1,t1);
		Arc a2 = new Arc(1,p1,0,t2);
		t1.getArcList().add(a1);
		t2.getArcList().add(a2);
		t.Add(a1);
		t.Add(a2);
		t.Add(p1);
		t.Add(t2);
		t.Add(t1);
		String res = t.printRDP();
		String resAttendus = "-------------------------------------------------------\n"
				+ "Réseau de Petri\n"
				+ "  1 places\n"
				+ "  2 transitions\n"
				+ "  2 arcs\n"
				+ "\n"
				+ "Liste de places :\n"
				+ "  place P0 avec 1.0 jetons\n"
				+ "\n"
				+ "Liste des Transitions : \n"
				+ "  transition T0\n"
				+ "  transition T1\n"
				+ "\n"
				+ "Liste des Arcs :\n"
				+ "  Arc       n·0 T0 - 1.0 -> P0\n"
				+ "  Arc       n·1 P0 - 1.0 -> T1\n"
				+ "-------------------------------------------------------";
		assertEquals(res,resAttendus);
	}
	
	@Test
	@Order(8)
	public void APe() throws Exception{
		ReseauPetri t = new ReseauPetri();
		Place p1 = new Place(-1,0);
		t.Add(p1);
		String res = t.printRDP();
		String resAttendus = "-------------------------------------------------------\n"
				+ "Réseau de Petri\n"
				+ "  1 places\n"
				+ "  0 transitions\n"
				+ "  0 arcs\n"
				+ "\n"
				+ "Liste de places :\n"
				+ "  place P0 avec 1.0 jetons\n"
				+ "\n"
				+ "Liste des Transitions : \n"
				+ "\n"
				+ "Liste des Arcs :\n"
				+ "-------------------------------------------------------";
		assertEquals(res,resAttendus);
	}
	@Test
	@Order(9)
	public void AA1() throws Exception{
		ReseauPetri t = new ReseauPetri();
		Place p1 = new Place(4,0);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(1,p1,0,t1);
		t1.getArcList().add(a1);
		t.Add(p1);
		t.Add(a1);
		t.Add(t1);
		String resAttendus = "-------------------------------------------------------\n"
				+ "Réseau de Petri\n"
				+ "  1 places\n"
				+ "  1 transitions\n"
				+ "  1 arcs\n"
				+ "\n"
				+ "Liste de places :\n"
				+ "  place P0 avec 4.0 jetons\n"
				+ "\n"
				+ "Liste des Transitions : \n"
				+ "  transition T0\n"
				+ "\n"
				+ "Liste des Arcs :\n"
				+ "  Arc       n·0 P0 - 1.0 -> T0\n"
				+ "-------------------------------------------------------";
		String res = t.printRDP();
		assertEquals(res,resAttendus);
	}

	@Test
	public void AAe1() throws Exception{
		ReseauPetri t = new ReseauPetri();
		Place p1 = new Place(4,0);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(-1,p1,0,t1);
		t1.getArcList().add(a1);
		t.Add(p1);
		t.Add(a1);
		t.Add(t1);
		String resAttendus = "-------------------------------------------------------\n"
				+ "Réseau de Petri\n"
				+ "  1 places\n"
				+ "  1 transitions\n"
				+ "  1 arcs\n"
				+ "\n"
				+ "Liste de places :\n"
				+ "  place P0 avec 4.0 jetons\n"
				+ "\n"
				+ "Liste des Transitions : \n"
				+ "  transition T0\n"
				+ "\n"
				+ "Liste des Arcs :\n"
				+ "  Arc       n·0 P0 - 1.0 -> T0\n"
				+ "-------------------------------------------------------";
		String res = t.printRDP();
		assertEquals(res,resAttendus);
	}
	
	@Test
	public void RPV() throws Exception{
		ReseauPetri t = new ReseauPetri();
		String resAttendus ="-------------------------------------------------------\n"
				+ "Réseau de Petri vide!!\n"
				+ "-------------------------------------------------------\n";
		String res = t.printRDP();
		assertEquals(res,resAttendus);
	}
	
	@Test
	public void RPVR() throws Exception{
		ReseauPetri t = new ReseauPetri();
		Place p1 = new Place(4,0);
		Transition t1 = new Transition(0);
		Arc a1 = new ArcReset(p1,t1);
		Arc a2 = new ArcZero(1,p1,t1);
		t.Add(a2);
		t.Add(a1);
		t.Add(p1);
		t.Add(t1);
		String res = t.printRDP();
		String resAttendus = "-------------------------------------------------------\n"
				+ "Réseau de Petri\n"
				+ "  1 places\n"
				+ "  1 transitions\n"
				+ "  2 arcs\n"
				+ "\n"
				+ "Liste de places :\n"
				+ "  place P0 avec 4.0 jetons\n"
				+ "\n"
				+ "Liste des Transitions : \n"
				+ "  transition T0\n"
				+ "\n"
				+ "Liste des Arcs :\n"
				+ "  ArcZero   n·0 P0 - 1.0 -> T0\n"
				+ "  ArcVideur n·1 P0 - Infinity -> T0\n"
				+ "-------------------------------------------------------";
		assertEquals(res,resAttendus);
		
	}
}