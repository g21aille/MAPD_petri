package JUnit;

import Classes.*;
import org.junit.Assert.*;
import org.junit.Assert.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Order;

public class TestPlace {
	@Test
	public void Constructor() throws Exception{
		Place p = new Place(1,1);
		assertFalse(p == null);
	}
	
	@Test
	public void Getter() throws Exception{
		Place p = new Place(1,1);
		assertEquals((int) p.getTokens(), 1);
	}
	
	@Test
	public void Setter() throws Exception{
		Place p = new Place(1,1);
		assertEquals((int) p.getTokens(), 1);
		p.setTokens(2);
		assertEquals((int) p.getTokens(), 2);
		p.setTokens(-2);
		assertEquals((int) p.getTokens(), 0);
	}
	
}
