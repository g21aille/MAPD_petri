package org.pneditor.petrinet.adapters.imta;

import org.pneditor.petrinet.adapters.imta.*;
import org.pneditor.petrinet.AbstractArc;
import org.pneditor.petrinet.AbstractNode;
import org.pneditor.petrinet.ResetArcMultiplicityException;
import org.pneditor.petrinet.AbstractPlace;

import Classes.Arc;

public class ArcNormalAdapter extends AbstractArc {
	
	private Arc a;
	private AbstractNode source;
	private AbstractNode destination;
	
	public Arc getArc() {
		return this.a;
	}
	
	public ArcNormalAdapter(AbstractNode source,AbstractNode destination) {
		this.source = source;
		this.destination = destination;
		
		if(source.isPlace()) {
			this.a = new Arc(1,((PlaceAdapter) source).getPlace(),0,((TransitionAdapter) destination).getTransition());
		}
		else {
			this.a = new Arc(1,((PlaceAdapter) destination).getPlace(),1,((TransitionAdapter) source).getTransition());
		}
		
	}
	@Override
	public AbstractNode getSource(){
		return this.source;
	}

	@Override
	public AbstractNode getDestination() {
		return this.destination;
	}

	@Override
	public boolean isReset() {
		return false;
	}

	@Override
	public boolean isRegular() {
		return true;
	}

	@Override
	public boolean isInhibitory() {
		return false;
	}

	@Override
	public int getMultiplicity() throws ResetArcMultiplicityException {
		return (int) a.getWeigth();
	}

	@Override
	public void setMultiplicity(int multiplicity) throws ResetArcMultiplicityException {
		a.setWeigth(multiplicity);
		
	}

}
