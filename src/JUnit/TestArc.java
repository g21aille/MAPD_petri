package JUnit;

import Classes.*;
import org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Order;


public class TestArc {
	
	@Test
	public void TestSetWeight() {
		Place p1 = new Place(3,1);
		Transition t1 = new Transition(1);
		Arc a = new Arc(1,p1,0,t1);
		assertEquals((int) a.getWeigth(),1);
		a.setWeigth(0);
		assertEquals((int) a.getWeigth(),0);
		a.setWeigth(-1);
		assertEquals((int) a.getWeigth(),0);
	}
	
	@Test
	public void TestSetSens() {
		Place p1 = new Place(3,1);
		Transition t1 = new Transition(1);
		Arc a = new Arc(1,p1,0,t1);
		assertEquals(a.getSens(),0);
		a.setSens(1);
		assertEquals(a.getSens(),1);
	}
	
	@Test
	public void TestSetPlace() {
		Place p1 = new Place(3,1);
		Transition t1 = new Transition(1);
		Arc a = new Arc(1,null,0,t1);
		assertEquals(a.getPlace(),null);
		a.setPlace(p1);
		assertEquals(a.getPlace(),p1);
	}
	
	@Test
	public void TestSetTransition() {
		Place p1 = new Place(3,1);
		Transition t1 = new Transition(1);
		Arc a = new Arc(1,p1,0,null);
		assertEquals(a.getTransition(),null);
		a.setTransition(t1);
		assertEquals(a.getTransition(),t1);
	}
	
	@Test
	public void TestGetSource() {
		Place p1 = new Place(3,1);
		Transition t1 = new Transition(1);
		Arc a = new Arc(1,p1,0,t1);
		assertEquals(a.getSource(),p1);
		a.setSens(1);;
		assertEquals(a.getSource(),t1);
	}
	@Test
	public void TestGetDestination() {
		Place p1 = new Place(3,1);
		Transition t1 = new Transition(1);
		Arc a = new Arc(1,p1,0,t1);
		assertEquals(a.getDestination(),t1);
		a.setSens(1);
		assertEquals(a.getDestination(),p1);
		
		
	}
}
