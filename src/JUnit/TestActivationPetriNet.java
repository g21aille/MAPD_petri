package JUnit;
import Classes.*;
import org.junit.Assert.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Order;


public class TestActivationReseau {
	
private ReseauPetri rp;
	
	@Before // Setup()
	public void before() throws Exception{
		rp = new ReseauPetri();
		Place p1 = new Place(1,0);
		Place p2 = new Place(1,1);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(1,p1,1,t1);
		Arc a2 = new Arc(1,p2,0,t1);
		t1.getArcList().add(a1);
		t1.getArcList().add(a2);
		rp.Add(a1);
		rp.Add(a2);
		rp.Add(p1);
		rp.Add(p2);
		rp.Add(t1);
	}
	
	@Test
	public void RD3() throws Exception{
		ReseauPetri r = new ReseauPetri();
		Place p1 = new Place(2,0);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(3,p1,0,t1);
		t1.getArcList().add(a1);
		r.Add(a1);
		r.Add(p1);
		r.Add(t1);
		r.Fire(t1);
		assertEquals(p1.getTokens(),2);
	}
	
	@Test
	public void RD5() throws Exception{
		ReseauPetri r = new ReseauPetri();
		Place p1 = new Place(2,0);
		Transition t1 = new Transition(0);
		Arc a1 = new ArcReset(p1,t1);
		t1.getArcList().add(a1);
		r.Add(a1);
		r.Add(p1);
		r.Add(t1);
		r.Fire(t1);
		assertEquals(p1.getTokens(),0);
	}
	
	@Test
	public void RM8() throws Exception{
		ReseauPetri r = new ReseauPetri();
		Place p1 = new Place(3,0);
		Place p2 = new Place(0,1);
		Transition t1 = new Transition(0);
		Arc a1 = new ArcReset(p1,t1);
		Arc a2 = new Arc(1,p2,1,t1);
		r.Add(p2);
		r.Add(a2);
		r.Add(a1);
		r.Add(p1);
		r.Add(t1);
		r.Fire(t1);
		assertEquals(p1.getTokens(),0);
		assertEquals(p2.getTokens(),1);
	}
	
	@Test
	public void RDM3() throws Exception{
		ReseauPetri r = new ReseauPetri();
		Place p1 = new Place(4,0);
		Place p2 = new Place(1,1);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(1,p2,0,t1);
		Arc a2 = new Arc(3,p1,0,t1);
		r.Add(p2);
		r.Add(a2);
		r.Add(a1);
		r.Add(p1);
		r.Add(t1);
		r.Fire(t1);
		assertEquals(p1.getTokens(),1);
		assertEquals(p2.getTokens(),0);
	}
	
	@Test
	public void RDM7() throws Exception{
		ReseauPetri r = new ReseauPetri();
		Place p1 = new Place(4,0);
		Place p2 = new Place(1,1);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(1,p2,0,t1);
		Arc a2 = new ArcReset(p1,t1);
		r.Add(p2);
		r.Add(a2);
		r.Add(a1);
		r.Add(p1);
		r.Add(t1);
		r.Fire(t1);
		assertEquals(p1.getTokens(),0);
		assertEquals(p2.getTokens(),0);
	}
	
	@Test
	public void RM4() throws Exception{
		ReseauPetri r = new ReseauPetri();
		Place p1 = new Place(2,0);
		Place p2 = new Place(1,1);
		Place p3 = new Place(0,2);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(1,p2,0,t1);
		Arc a2 = new Arc(3,p1,0,t1);
		Arc a3 = new Arc(1,p3,1,t1);
		r.Add(a3);
		r.Add(p3);
		r.Add(p2);
		r.Add(a2);
		r.Add(a1);
		r.Add(p1);
		r.Add(t1);
		r.Fire(t1);
		assertEquals(p1.getTokens(),2);
		assertEquals(p2.getTokens(),1);
		assertEquals(p3.getTokens(),0);
	}
	
	@Test
	public void RM9() throws Exception{
		ReseauPetri r = new ReseauPetri();
		Place p1 = new Place(2,0);
		Place p2 = new Place(1,1);
		Place p3 = new Place(0,2);
		Transition t1 = new Transition(0);
		Arc a1 = new Arc(1,p2,0,t1);
		Arc a2 = new ArcReset(p1,t1);
		Arc a3 = new Arc(1,p3,1,t1);
		r.Add(a3);
		r.Add(p3);
		r.Add(p2);
		r.Add(a2);
		r.Add(a1);
		r.Add(p1);
		r.Add(t1);
		r.Fire(t1);
		assertEquals(p1.getTokens(),0);
		assertEquals(p2.getTokens(),0);
		assertEquals(p3.getTokens(),1);
	}
	
}
