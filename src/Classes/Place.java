package Classes;

public class Place {

	private double nbTokens;
	private int finalID;
	/**
	 * @param nbTokens
	 * @param finalID
	 */
	public Place(double nbTokens, int finalID) {
		this.nbTokens = nbTokens;
		if(nbTokens < 0) {
			this.nbTokens = Math.abs(nbTokens);
		}
		this.finalID = finalID;
	}
	public double getTokens() {
		return this.nbTokens;
	}
	public void setTokens(double nbTokens) {
		if(nbTokens < 0) {
			this.nbTokens = 0;
			return;
		}
		this.nbTokens = nbTokens;
	}
	public int getID() {
		return this.finalID;
	}
	
}
