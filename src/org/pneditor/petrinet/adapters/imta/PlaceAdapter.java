package org.pneditor.petrinet.adapters.imta;
import org.pneditor.petrinet.AbstractPlace;
import Classes.Place;

public class PlaceAdapter extends AbstractPlace{

	private Place place;
		
	public PlaceAdapter(String label) {
		super(label);
		this.place = new Place(0,Integer.parseInt(label));
	}

	public Place getPlace(){
		return this.place;
	}
	@Override
	public void addToken() {
		this.place.setTokens(this.place.getTokens() + 1 );
		return;
		
	}

	@Override
	public void removeToken() {
		this.place.setTokens(this.place.getTokens() - 1 );
		return;		
	}

	@Override
	public int getTokens() {
		return (int) this.place.getTokens();
	}

	@Override
	public void setTokens(int tokens) {
		this.place.setTokens(tokens);
		
	}

}
