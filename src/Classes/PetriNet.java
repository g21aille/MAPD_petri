package Classes;
import java.util.LinkedList;

public class ReseauPetri implements InterfaceReseauPetri{
	private LinkedList<Arc> arcList;
	private LinkedList<Transition> transitionList;
	private LinkedList<Place> placeList;
	
	public ReseauPetri() {
		this.arcList = new LinkedList<Arc>();
		this.placeList = new LinkedList<Place>();
		this.transitionList  = new LinkedList<Transition>();
	}
	public LinkedList<Arc> getArcList(){
		return this.arcList;
	}
	public LinkedList<Transition> getTransitionList(){
		return this.transitionList;
	}
	public LinkedList<Place> getPlaceList() {
		return this.placeList;
	}
	@Override
	public boolean Fire(Transition t) {
		if(t.isActivable()) {
			for(Arc a : t.getArcList()) {
				int s = a.getSens();
				if(s == 0) {
					a.remooveTokens();
				}
				else {
					a.addTokens();
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean Add(Transition t) {
		return this.transitionList.add(t);
		
	}

	@Override
	public boolean Add(Place p) {
		// TODO Auto-generated method stub
		return  this.placeList.add(p);
	}

	@Override
	public boolean Add(Arc an) {
		an.getTransition().addArcToList(an);
		return this.arcList.add(an);
		
	}

	@Override
	public boolean Remove(Arc an) {
		if(an.getTransition() != null){
		an.getTransition().getArcList().remove(an);
		};
		return this.arcList.remove(an);
	}

	@Override
	public boolean Remove(Place p) {
		LinkedList<Arc> listArc = new LinkedList<Arc>();
		for(int i = 0; i < this.arcList.size(); i++) {
			if(this.arcList.get(i).getPlace().equals(p)) {
				listArc.add(this.arcList.get(i));
			}
		}
		for(int i = 0; i < listArc.size(); i++){
			this.Remove(listArc.get(i));
		}
		
		return this.placeList.remove(p);
	}

	@Override
	public boolean Remove(Transition t) {
		while(t.getArcList().size() != 0){
			this.Remove(t.getArcList().pop());
		}
		return this.transitionList.remove(t);
	}

	@Override
	public boolean ChangeWigth(Arc an,int wgth) {
		if(wgth< 0) {
			return false;
		}
		if(this.arcList.contains(an)) {
			int i = this.arcList.indexOf(an);
			Arc a = this.arcList.get(i);
			a.setWeigth(wgth);
			return true;
		}
		return false;
		
	}

	@Override
	public boolean ChangeTokens(Place p,int tkns) {
		if(tkns< 0) {
			return false;
		}
		if(this.placeList.contains(p)) {
			int i = this.placeList.indexOf(p);
			Place p1 = this.placeList.get(i);
			p1.setTokens(tkns);
			return true;
		}
		return false;
		
	}
	
	public String printRDP() {
		String res = "";
		Arc a = new Arc(0,null,0,null);
		ArcReset av = new ArcReset(null,null);
		ArcZero az = new ArcZero(0,null,null);
		res += "-------------------------------------------------------\n";
		if(placeList.size() == 0 && transitionList.size()==0 && arcList.size()==0) {
			res += "Réseau de Petri vide!!\n";
			res += "-------------------------------------------------------\n";
			return res;
		}
		res += "Réseau de Petri\n";
		res += "  " + placeList.size()+" places\n";
		res += "  " + transitionList.size()+" transitions\n";
		res += "  " + arcList.size()+" arcs\n";
		res += "\n";
		res += "Liste de places :\n";
		for(int i = 0; i < placeList.size(); i++) {
			res += "  place P" + i+" avec "+placeList.get(i).getTokens()+" jetons\n";
		}
		res += "\n";
		res += "Liste des Transitions : \n";
		for(int i = 0; i < transitionList.size(); i++) {
			res += "  transition T" +i+"\n";
		}
		
		res += "\n";
		res += "Liste des Arcs :\n";
		for(int i = 0; i < arcList.size(); i++) {
			String typeArc = "";
			if(arcList.get(i).getClass().equals(a.getClass())) {
				typeArc = "Arc      ";
			}
			if(arcList.get(i).getClass().equals(av.getClass())) {
				typeArc = "ArcVideur";
			}
			if(arcList.get(i).getClass().equals(az.getClass())) {
				typeArc = "ArcZero  ";
			}
			if(arcList.get(i).getSens() == 0) {
				res += "  "+typeArc+" n·"+i+ " P"+arcList.get(i).getPlace().getID()+" - "+arcList.get(i).getWeigth()+" -> T"+arcList.get(i).getTransition().getID()+"\n";
			}
			else {
				res += "  "+typeArc+" n·"+i+ " T"+arcList.get(i).getTransition().getID()+" - "+arcList.get(i).getWeigth()+" -> P"+arcList.get(i).getPlace().getID()+"\n";
			}
			
		}
		res += "-------------------------------------------------------";
		return res;
	}

}
