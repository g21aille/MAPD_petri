package Classes;

public class ArcReset extends Arc {

	public ArcReset(Place place, Transition transition) {
		super(Double.POSITIVE_INFINITY, place, 0, transition);
	}
	public void removeTokens(Place place) {
		this.getPlace().setTokens(0);
		
	}
	public boolean isActivable() {
		if (this.getPlace().getTokens() > 0) {
			return true;
		}
		return false;
	}

}
