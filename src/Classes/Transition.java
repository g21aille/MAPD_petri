package Classes;
import java.util.LinkedList;

public class Transition {
	private int finalID;
	private LinkedList<Arc> arcList;
	/**
	 * @param finalID
	 * @param arcList
	 */
	public Transition(int finalID) {
		this(finalID, new LinkedList<Arc>());
	}
	public Transition(int finalID, LinkedList<Arc> arcList) {
		this.finalID = finalID;
		this.arcList = arcList;
	}
	/**
	 * @return the arcList
	 */
	public LinkedList<Arc> getArcList() {
		return arcList;
	}
	/**
	 * @param arcList the arcList to set
	 */
	public void setArcList(LinkedList<Arc> arcList) {
		this.arcList = arcList;
	}
	public void addArcToList(Arc a) {
		this.arcList.add(a);
	}
	/**
	 * @return the finalID
	 */
	public int getID() {
		return finalID;
	}
	public boolean isActivable() {
        for (int i = 0; i < this.arcList.size(); i++) {
        	if (this.arcList.get(i).isActivable() == false) {
        		System.out.print(arcList.get(i).getDestination());
        		return false;
        	}
        
	}
        return true;
	
}
	}
