package org.pneditor.petrinet.adapters.imta;

import org.pneditor.petrinet.AbstractArc;
import org.pneditor.petrinet.AbstractNode;
import org.pneditor.petrinet.AbstractPlace;
import org.pneditor.petrinet.AbstractTransition;
import org.pneditor.petrinet.PetriNetInterface;
import org.pneditor.petrinet.ResetArcMultiplicityException;
import org.pneditor.petrinet.UnimplementedCaseException;

import Classes.Place;
import Classes.ReseauPetri;

public class PetriNetAdapter extends PetriNetInterface{
	
	private ReseauPetri rp;
	
	public PetriNetAdapter() {
		this.rp = new ReseauPetri();
	}
	@Override
	public AbstractPlace addPlace() {
		PlaceAdapter p = new PlaceAdapter(String.valueOf(this.rp.getPlaceList().size()));
		rp.Add(p.getPlace());
		return p;
		
	}

	@Override
	public AbstractTransition addTransition() {
		TransitionAdapter t = new TransitionAdapter(String.valueOf(this.rp.getTransitionList().size()));
		rp.Add(t.getTransition());
		return t;
	}

	@Override
	public AbstractArc addRegularArc(AbstractNode source, AbstractNode destination) throws UnimplementedCaseException {
		ArcNormalAdapter a = new ArcNormalAdapter(source,destination);
		System.out.print(a.getSource());
		rp.Add(a.getArc());
		return a;
	}

	@Override
	public AbstractArc addInhibitoryArc(AbstractPlace place, AbstractTransition transition)
			throws UnimplementedCaseException {
		ArcZeroAdapter a = new ArcZeroAdapter(place,transition);
		rp.Add(a.getArc());
		return a;}

	@Override
	public AbstractArc addResetArc(AbstractPlace place, AbstractTransition transition)
			throws UnimplementedCaseException {
		ArcVideurAdapter a = new ArcVideurAdapter(place,transition);
		rp.Add(a.getArc());
		return a;}
	@Override
	public void removePlace(AbstractPlace place) {
		rp.Remove(((PlaceAdapter) place).getPlace());
		
	}

	@Override
	public void removeTransition(AbstractTransition transition) {
		rp.Remove(((TransitionAdapter) transition).getTransition());
		
	}

	@Override
	public void removeArc(AbstractArc arc) {
		rp.Remove(arc.getArc());
		
	}

	@Override
	public boolean isEnabled(AbstractTransition transition) throws ResetArcMultiplicityException {
		System.out.print(((TransitionAdapter) transition).getTransition().isActivable());
		return ((TransitionAdapter) transition).getTransition().isActivable();}

	
	@Override
	public void fire(AbstractTransition transition) throws ResetArcMultiplicityException {
		rp.Fire(((TransitionAdapter) transition).getTransition());
		
	}
	

	

}
