package JUnit;
import Classes.*;



import org.junit.Assert.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Order;


public class TestReseauPetri {
	
	@Test// CP1()
	public void CP1() throws Exception{
		Place p = new Place(1,0);
		assertEquals(1,p.getTokens());
	}
	
	@Test// CP2()
	public void CP2() throws Exception{
		Place p = new Place(-1,0);
		assertEquals(1,p.getTokens());
	}
	@Test
	public void CA1() throws Exception{
		Place p = new Place(-1,0);
		Arc a = new Arc(3,p,1,null);
		assertEquals(a.getDestination().getClass(),p.getClass());
	}
	@Test
	public void CAD0() throws Exception{
		Place p = new Place(-1,0);
		Transition t = new Transition(0);
		Arc a = new Arc(3,p,0,t);
		Arc a1 = new Arc(2,p,0,t);
		t.getArcList().add(a);
		t.getArcList().add(a1);
		assertEquals(a.getDestination().getClass(),t.getClass());
		assertEquals(a1.getDestination().getClass(),t.getClass());
		assertEquals(a.getDestination(),a1.getDestination());
		assertEquals(a.getSource(),a1.getSource());
	}
	@Test
	public void CAV() throws Exception{
	Arc a1 = new ArcReset(null,null);
	assertEquals(Double.POSITIVE_INFINITY,a1.getWeigth());
	assertEquals(0,a1.getSens());
	}
	
	@Test
	public void CAZ() throws Exception{
	Arc a1 = new ArcZero(0, null,null);
	assertEquals(0,a1.getSens());
	}
	
	@Test
	public void CAJ0() throws Exception{
		Place p = new Place(3,0);
		Arc a = new Arc(1,p,1,null);
		a.addTokens();
		assertEquals(4,p.getTokens());
	}
	
	@Test
	public void CEJ0() throws Exception{
		Place p = new Place(1,0);
		Arc a = new Arc(3,p,1,null);
		a.remooveTokens();
		assertEquals(0,p.getTokens());
	}
	
	
	@Test
	public void CEJ1() throws Exception{
		Place p = new Place(4,0);
		Arc a = new Arc(3,p,1,null);
		a.remooveTokens();
		assertEquals(1,p.getTokens());
	}
	
	@Test
	public void TestChangeWeight() {
		ReseauPetri rp = new ReseauPetri();
		Place p = new Place(0,0);
		Transition t = new Transition(1);
		Arc a = new Arc(1,p,0,t);
		Arc b = new Arc(1,p,0,t);
		rp.Add(a);
		rp.Add(p);
		rp.Add(t);
		assertEquals((int) a.getWeigth(),1);
		rp.ChangeWigth(a, 0);
		assertEquals((int) a.getWeigth(),0);
		rp.ChangeWigth(a, -1);
		assertEquals((int) a.getWeigth(),0);
		assertEquals(rp.ChangeWigth(b, 0),false);
	}
	
	@Test
	public void TestChangeTokens() {
		ReseauPetri rp = new ReseauPetri();
		Place p = new Place(0,0);
		Place p2 = new Place(1,0);
		rp.Add(p);
		assertEquals((int) p.getTokens(),0);
		rp.ChangeTokens(p, 1);
		assertEquals((int) p.getTokens(),1);
		rp.ChangeTokens(p, -1);
		assertEquals((int) p.getTokens(),1);
		assertEquals(rp.ChangeTokens(p2, 2),false);
	}
 
}
