package Classes;
import java.util.LinkedList;

public interface InterfaceReseauPetri {
	
	public boolean Fire(Transition t);
	public boolean Add(Transition t);
	public boolean Add(Place p);
	public boolean Add(Arc an);
	public boolean Remove(Arc an);
	public boolean Remove(Place p);
	public boolean Remove(Transition t);
	public boolean ChangeWigth(Arc an,int wgth);
	public boolean ChangeTokens(Place p,int tkns); 
}
