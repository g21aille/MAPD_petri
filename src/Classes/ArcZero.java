package Classes;

public class ArcZero extends Arc {
	
	public ArcZero(double weight, Place place, Transition transition) {
		super(weight, place, 0, transition);
	}
	
	public boolean isActivable() {
		if (this.getPlace().getTokens() == 0) {
			return true;
		}
		return false;
	}

}
