package JUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.LinkedList;

import org.junit.Test;

import Classes.*;
import org.junit.Assert.*;
import org.junit.Assert.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Order;
public class TestTransition {

	@Test
	public void Constructor() throws Exception{
		Transition p = new Transition(1);
		assertFalse(p == null);
	}
	
	@Test
	public void TestActivable() throws Exception{
		Transition t = new Transition(1);
		assertTrue(t.isActivable() == true);
		Place p = new Place(1,1);
		Arc a = new Arc(1,p,0,t);
		t.addArcToList(a);
		assertTrue(t.isActivable() == true);
		p.setTokens(0);
		assertTrue(t.isActivable() == false);
	}
		
	

	@Test
	public void TestSetList() throws Exception{
		Transition t = new Transition(1);
		LinkedList<Arc> arcList = new LinkedList<Arc>();
		Place p = new Place(1,0);
		Arc a = new Arc(1,p,0,t);
		arcList.add(a);
		t.setArcList(arcList);
		assertTrue(t.getArcList().equals(arcList));
		
	}
}
