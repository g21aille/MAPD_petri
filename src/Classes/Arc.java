package Classes;

public class Arc {
	private double weigth;
	private int sens;
	private Place place;
	private Transition transition;
	
	public Arc(double weigth, Place place, int sens, Transition transition){
		this.weigth = weigth;
		if(weigth < 0) {
			this.weigth = Math.abs(weigth);
		}
		this.place = place;
		this.transition= transition;
		this.sens = sens;
		
	}

	/**
	 * @return the weigth
	 */
	public double getWeigth() {
		return weigth;
	}

	/**
	 * @param weigth the weigth to set
	 */
	public void setWeigth(double weigth) {
		if(weigth < 0) {
			this.weigth = 0;
			return;
		}
		this.weigth = weigth;
	}

	/**
	 * @return the sens
	 */
	public int getSens() {
		return sens;
	}

	/**
	 * @param sens the sens to set
	 */
	public void setSens(int sens) {
		this.sens = sens;
	}

	/**
	 * @return the place
	 */
	public Place getPlace() {
		return place;
	}

	/**
	 * @param place the place to set
	 */
	public void setPlace(Place place) {
		this.place = place;
	}

	/**
	 * @return the transition
	 */
	public Transition getTransition() {
		return transition;
	}

	/**
	 * @param transition the transition to set
	 */
	public void setTransition(Transition transition) {
		this.transition = transition;
	}
	
	public void remooveTokens() {
		Place p = this.getPlace();
		double tokens = p.getTokens();
		if(tokens < this.getWeigth()) {
			p.setTokens(0);
		}
		else {
			p.setTokens(tokens - this.getWeigth());
		}
	}
	
	public void addTokens() {
		this.getPlace().setTokens(this.getPlace().getTokens() + this.getWeigth());
	}
	
	public Object getDestination() {
		if(this.getSens() == 0) {
			return this.getTransition();
		}
		else {
			return this.getPlace();
		}
	}
	public Object getSource() {
		if(this.getSens() == 0) {
			return this.getPlace();
		}
		else {
			return this.getTransition();
		}
	}
	public boolean isActivable() {
		if(this.getSens() == 1) {
			return true;
		}
		else {
			if(this.getPlace().getTokens() >= this.getWeigth()) {
				return true;
			}
			else {
				return false;
			}
		}
	}
}
